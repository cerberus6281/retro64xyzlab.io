# Contributing

Thank you for your consideration in contributing to this site. Whether you wish
to make changes to the back-end code or contribute an article or other piece of
literature for post on this site, this guide will assist you in doing it
correctly.

Please be cognizant that this project has specific standards and expectations
for contributors. You can meet those standards by simply using previously
published articles as a solid base.
