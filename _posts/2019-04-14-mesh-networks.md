---
layout: post
title: "Introduction To Mesh Networks"
date: 2019-04-14T04:52:43-07:00
author: RetroMe
summary: >
 Introduction To Mesh Networks is a gentle introduction to off grid
 communication, the PirateBox project, and deploying simple hardware to
 fulfill communication locally without the internet. 
categories: presentations
thumbnail: fa-desktop
tags:
 - mesh networking
 - privacy
 - security
 - development
 - phoenix linux users group
---

## Performance Objective

At the conclusion of the course the student will be able to:

1. Identify what a mesh network is.

2. Identify how a mesh network can be employed during an emergency.

3. Identify what the PirateBox project is.

4. Describe one feature that the PirateBox provides.

## Introduction

A wireless mesh network or (WMN) is a method of creating a network for
communication that is composed of radio nodes that act in an ad hoc manner. The
average WMN will consit of clients, routers, and gateways that allow the
devices connected to the network to communicate with each other. This is
usually designed to work without any kind of centralized infrastructure that
would prevent communication if any one node were to disappear. However, on
smaller networks it is possible that the inoperability of a single device could
cease communications for all members involved. This is likely when deploying a
PirateBox for communication. If you have a single PirateBox and that PirateBox
fails, you will quickly find yourself without a means to communicate.

The normal internet functions by providing a centralized infrastructure that is
designed to ferry data and information around the globe at high speeds. It is
also useful for allowing people to monitor and spy on every individual using
that infrastructure. This spying can take the form of capturing of packets and
reading the exact information being sent or it can include more esoteric spying
such as the passing of digital fingerprints and measuring traffic to make a
best guess as to the content of the traffic itself.

PirateBox can be used to spy on traffic but the spirit of the device and the
intention in which it is deployed reduces that possibility. A PirateBox creates
a very small and local network that can be accessed by present parties to share
information and foster digital communication. Similar to the bulletin board
systems of days gone past, the PirateBox is tool that can enhance local
communication and to foster discussion within small communities.

## PirateBox Goals

Reproduced without permission, the below list contains the stated goals of the
[PirateBox project][pboxgoals].


1. PirateBox provides easy file sharing and messaging over a local area
   network.

2. PirateBox is designed to educate the public and make them aware about
   online censorship, surveillance, and freedom of speech.

3. PirateBox truly respects privacy and thus does not collect any access or
   connection logs from its users.

4. PirateBox has an educational purpose and aims to teach about computer
   programming and computer networks.

5. PirateBox network (AP) should be open, and use open networking standards
   to allow communication with other users.

### File Sharing And Messaging

File sharing and messaging are extremely important to anyone who has any amount
of experience with the internet. The ability to relay data and information with
rapidity is vital when dealing with emergencies or even with something as
simple as requesting homework help. The internet itself lives on the vital life
force of files and text that is pumped between device on the network.

PirateBox fosters file sharing and messaging through the inclusion of a simple
browser based application that exposes file sharing, chat, and an image board
to users who connect to the PirateBox network. This behavior is the core of the
PirateBox user interface and is vital for encouraging the use of the device.

### Education

PirateBox has a stated goal of educating the public on censorship,
surveillance, and freedom of speech. Education of the public is vitally
important. PirateBox is an excellent conversation piece and demonstration of
how a free and open internet can function and is an excellent tool for
generating discussion on topics that the public may have difficulty even asking. 

PirateBox is also beneficial in that it encourages tinkering and exploration.
Users who deploy a PirateBox may look for ways to improve the project or to
find alternatives that better fit their use case scenario. Can I improve
connectivity by adding new antennas? Deploying more devices? Adding a method to
support different types of encryption for different layers of use? All of these
questions could come up and users might look for answers.

### Privacy

Internet surveillance is a topic that raises serious concern for both law
enforcement as well as citizenry. The public perception of law enforcement is
that they should be able to solve crimes, stop crimes, resolve issues after
crimes have occurred, and do all of this without ever making a mistake or
causing an issue. They must also do all of this without ever glancing at
anything that others have done or posted online and must respect the privacy of
all peoples but must also stop terrorists from conducting attacks and organizing
online. Internet based surveillance is not going away and will continue to grow
thanks to the number of always connected online enabled devices that exist in
homes and on persons nearly everywhere.

Some individuals seem to believe that they have a right to privacy when they
share their information because there is a continued notion that companies like
Facebook, Twitter, or Reddit have your best interests. This is false. These
companies use you as a product that they distill, condense, package, and sell
for a profit while expecting you to constantly produce content for them to data
mine. Every thing you upload to the internet through these companies should be
considered compromised by every government, institution, group, and person on
this planet.

PirateBox removes you from the digital data mining circle and encourages you to
develop your own methods of communication. You can operate independently of
these companies and are encouraged to do so. If you wish to use a device in a
manner in which only people you trust are allowed to communicate with you, you
can.

### Open Standards

Open Source is not Free Software. Richard Stallman states that open source
software is released as a practical advantage that does not campaign for
principles. Free software respects a users ability to run it, study it, change
it, and to redistribute copies with or without changes. Open Standards are the
policies and technology that exist in a non-proprietary manner while allowing
any other device to be able to communicate if they too share in those open
standards.

So does PirateBox profess to be free software friendly? No. It is an open
source project that exists to use open standards. This means that devices that
include proprietary drivers could potentially function. Is this good or bad?
Neither. It is an ideology and a design choice based on practicality. PirateBox
does not appear to be approved as Free Software Foundation friendly.

You can search for yourself at the [FSF Directory][fsfdir].

## Development

[Development][pboxdev] of the PirateBox is done using Github. The PirateBox
teams keeps a large number of repositories available for your perusal and their
website has excellent documentation on how to locate their projects. I
recommend reviewing their development repositories to build your knowledge of
how the device works, how to improve on it, and how to deploy your changes.

## Deployment

Deployment of the PirateBox provides infinite possibilities. You can pretend to
be a spy, setup a mesh LAN for your neighborhood, or provide an intranet for
use during an emergency. The PirateBox is an awesome tool.

1. Dead Drops

2. Emergency Operations

3. Meetups

### Dead Drops

The PirateBox allows for anonymous communication and file transfers between
multiple parties. You can also use the device for chat or as a forum. This
behavior is independent of the internet and does not require anything beyond
normal WIFI networking tools to function. An individual could setup a PirateBox
at a location, leave it connected to a battery or other form of power, and
allow individuals passing by to communicate with the device and leave messages
or other data at their leisure that could be later retrieved.

### Emergency Operations

The very nature of an emergency invariably means that the normal situation is
no more and we may require tools like the PirateBox to communicate in a grid
down or grid damaged event. Shelter standup could be conducted when there is no
access to the internet and a tool like the PirateBox could be deployed to allow
communication locally. Minor changes to the PirateBox could be made to provide
an announcements tool allowing users to connect and view information being
posted by emergency management personnel. This could include scheduling,
orders, or lists of casualties updated in real time. The PirateBox can be
[upgraded][propbox] to provide an amazing connection capability.

### Meetups

The PirateBox could also be a fun tool to stand up during meetings or
conventions. You can deploy the device and see who finds it and what they do
with it.  The very nature of the PirateBox will encourage interaction and
sharing.  Will someone attempt to upload malware? Photos of their pets? Or will
they use it to spread discord or chaos? No way to know until you stand one up
and wait and see what happens.

## Answers

1. A mesh network is a method by which infrastructure is deployed to allow
   bridges, switches, and other devices to communicate directly and
   non-hierarchically as possible with each other.

2. A mesh network can be deployed during an emergency to allow computers and
   other devices to communicate with each other over a wireless connection when
   the grid is damaged or otherwise unavailable.

3. The PirateBox is an anonymous offline mobile file-sharing and
   communications system that can be deployed using off-the-shelf hardware and
   free software.

4. The PirateBox provides an image board as well as file sharing.

## Conclusion

A mesh network is a vital tool for off grid communication and provides a
promising base for developing a local method of communication that is divorced
from standard infrastructure. Mesh networking is a defense against the control
exerted by centralized resources and their decision on what you may or may not
do with the internet.

Projects like the PirateBox can be privacy respecting as is claimed in the
goals of the project itself. These projects are often intended to help
individuals who are interested in removing themselves from the Facebook,
Google, and Ad powered ecosystem that most people are beholden to.

You as a user have an opportunity to contribute to a free internet by adding
mesh networking to your equipment or gear bag. You can use battery powered
Raspberry PI based devices as well as other alternatives to create and
distribute a powerful network that can host any number of files. Contribute to
a free internet by building a free internet.

## Final Recommendations

1. Choose *nix. 
2. Build or join a mesh network.
3. Network and build relationships in real life locally.
4. Develop your equipment and gear.
5. Choose freedom.

[pboxgoals]: https://piratebox.cc/goals 'Goals of the PirateBox Project'
[pboxdev]: https://piratebox.cc/development:git_repositories 'PirateBox Dev'
[propbox]: https://archive.fo/8cUh1 'Setting up a hard core PirateBox'
[fsfdir]: https://directory.fsf.org/wiki/Main_Page 'The FSF Directory'
