---                                                                             
layout: post
title: "Book Study - Your Most Powerful Weapon"
date: 2019-03-17T15:52:43-07:00
author: RetroMe
summary: >
  Your Most Powerful Weapon is a book by Steve Terani on mental toughness,
  environment control, threat avoidance, and fear control.
categories: books 
thumbnail: fa-book
tags:
 - book review
 - law enforcement
 - training
 - mental toughness
---

## A foreword

I received a copy of this book from my mentor. He encouraged me to read the
book as part of my leadership training and because he found it to be very
beneficial. I was unsure if this book was going to be of use to me but after
having read it, I was very impressed. It had a lot of good information and the
tone of the book was very familiar to me as I felt that there were many bits of
information that I had previously heard or otherwise been trained on in other
classes. 

The book claims that it will teach you how to: build mental toughness, control
your environment, avoid threats, defeat attackers, and control fear. Does it?

## Your Most Powerful Weapon 

**How To Use Your Mind To Stay Safe**

## The New Normal

Tarani touches on normalcy bias, types of terrorism, and the ubiquity of new
forms of attack that can be executed remotely. You must take responsibility for
your future and safety. There are two basic forms of attack, the lone wolf
style and the command or leadership enabled type.

## How To Control Your Environment

Situational awareness is the concept of knowing what is happening around you.
Many people do not pay attention to their surroundings and are not likely to
initiate action to protect themselves. Tarani references a study in which
images of individuals were shown to criminals and those criminals were polled
on who they felt was a 'good' choice of victim. Every criminal made the
unanimous decision that their target should be the person who appeared to be
unaware and not paying attention.

## How To Control A Threat

How you react to a threat is as important as threat avoidance. When you go to
guns, you have failed. You need to be able to react to situations as they
evolve and understand that the risks that come from dealing with an issue after
it is right of bang. You face injury both physical and liability related as
well as possible death when you have to confront a threat with force. It is
better to avoid the threat in it's entirety and defend yourself through not
being a target.

## How To Defeat An Attack

The first step in defeating a threat is to be able to recognize it and to
interdict the threat before you must resort to violence or force. Just as when
controlling the threat, you want to be able to reply with the least amount of
force necessary to gain the advantage and to hold it. Development of soft
skills is extremely important because it can provide you the abilities
necessary to defend yourself without having to move to violence.

## How To Control Fear

Many people are not inoculated against violence or fear. There is a popular
story about two air craft that collided while on a run way. The majority of
individuals on one plane died in a fire because they refused to get out of
their seats and exit the plane. They sat in stunned silence until overcome by
the smoke. You must not allow fear to rob you of your ability to care for
yourself or your loved ones. As an aside, I was taught that courage is not the
absence of fear but is the application of action when you are afraid.

## Conclusion

This is a fantastic book and I was extremely happy with the recommendation I
received to read it. A lot of the information consisted of solid advice that I
had previously received when I was working for the Texas State Guard. However,
it had been many years since I had last received this kind of training and the
refreshed was much appreciated. I would recommend that people read this book.
It will make a positive impact in your life whether you are military, police,
or civilian.

You can purchase the book on [Amazon][amazon].

[author]: https://stevetarani.com/ 'The Author Steve Tarani'
[amazon]: https://www.amazon.com/Your-most-Powerful-Weapon-Using/dp/B078YQ2D2F 'Amazon Link' 
