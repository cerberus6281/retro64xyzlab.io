---
layout: post
title: "Security Discussion - Church - March"
date: 2019-03-22T04:52:43-07:00
author: RetroMe
summary: >
  A one hour discussion on preparedness, terrorism, and security conducted for
  my church. I am the preparedness instructor and adviser for my church. We
  will be discussing terrorism, threats, and recommendations for safety.
categories: presentations
thumbnail: fa-desktop
tags:
  - church
  - physical security
  - terrorism
  - preparedness
---

# Topics

1. Violence In Houses Of Worship

2. First Aid Familiarity

3. Getting Organized - A Proposal

## Introduction

'The new normal' is an idea proposed by author and former CIA employee Steve
Tarani that violence and community fracture is inevitable as many different
groups and ideologies continue to clash. These clashes will spawn violence that
many individuals are not mentally prepared for. We must not allow normalcy
bias to rob us of our ability to react to these situations but instead must
embrace this new normal and prepare ourselves physically and mentally to react
when presented with danger. 

We need to understand that moving forward, if you are responding to 'Bang', you
have already failed. If you have to go to guns, you have failed. If you are not
prepared to avoid the threat instead of confronting it, you have failed.

## Christchurch Shooting

On March 15, 2019 a 28 year old man named Brenton Tarrant used Facebook to live
stream himself conducting a shooting at the Masjid Al Noor mosque in
Christchurch New Zealand at 1345. He used multiple weapons including rifles as
well as shotguns to conduct the shooting. His victims included men, women, and
children. The estimated death toll was 49 with more than 20 individuals
wounded. Many of his victims required multiple surgeries to save their lives.

Tarrant used Facebook to live stream his attack, posted numerous warnings
online, and also posted on a popular image board with information on his
upcoming attack and an invitation for viewers to view the carnage. He also
informed individuals that his intention was to live but he was expecting law
enforcement response to be deadly.

![Tarrant Wall Art][tarrant]

Tarrant has been celebrated both online as well as off line. His actions are
being called an inspiration for numerous people who feel that violence is the
only answer in response to what they believe to be increasing encroachment on
their way of life. Calls for further violence have been posted online with many
referring to Tarrant as a 'Saint'.

![Tarrant Weapon][tarrantweap]

![Tarrant Magazine][tarrantmags]

Tarrant decorated his weapons with eulogies and slogans. He posted pictures of
the weapons online and framed the weapons in his videos so that his messages
could be read while he conducted his violent attacks.

![isisweapon][isisweapon]

ISIS posted a photo of an AK-47 variant in a tweet with promises of
retaliation. The weapon was painted in a manner similar to how Tarrant painted
his weapons. The messages and memorials for the victims of Islamic attacks were
added to Tarrants weapon using a silver or white pen or marker.  This was
imitated by the ISIS fighter with his threat.

## Threats Against The Latter Day Saint Community

![isisthreat][isisthreat] 

Sometime around November 2017 members of ISIS began posting threatening
images of bloody knives, Christmas gatherings, and an iconic LDS Temple located
in San Diego with threats of violence. Law enforcement turn out was swift and
overwhelming and as of this posting there has not been an organized attack on
any LDS Temples within the United States in recent history.

## First Aid

Please consider creating a simple combat life saver kit for yourself and
family. An effective kit would include gloves, tourniquets (CAT for adults and
wrap style for children), and a simple pressure dressing. An effective kit can
be put together for very little money and will fit on a belt using a pouch. 

## Getting Organized

The Maricopa Sheriffs Office provides a block watch program as well as
encourages the organization of members of the community. I am not asking you to
do any policing and neither is the MCSO. I am asking you to be willing to be
organized and to keep an eye on your homes and neighborhood.

	One of the best deterrents against crime is positive outdoor activity. When
	the community is out and about, you can be the eyes and ears of the
	neighborhood, but please leave the policing to our deputies.

	Please notify MCSO of any suspicious activity by calling the non-emergency
	number, 602-876-1011; in an emergency, call 9-1-1. Not only does the
	notification help solve potential crimes and keep the neighborhood safe,
	the information assists with intelligence led policing. MCSO has a crime
	analyst who uses calls for service to help direct targeted patrols and
	identify trends.

I have requested information on the block watch and volunteer to organize it if
members of our community are willing to participate. You are not being asked to
become agents of enforcement but are being asked to be willing to report crime
and to secure your neighborhood through cooperation with local established law
enforcement channels.

## Recommended Reading

[Your Most Powerful Weapon][amazontarani] is a training manual on staying safe
in a world where terrorist attacks, active shootings, and physical violence are
the new normal. How can you keep your family safe without a firearm or knowing
a martial art?

[Steve Tarani][authortarani] the author of Your Most Powerful Weapon is a
former CIA full-time employee, protective services subject matter expert and
lead instructor for a 3 million member strong training program for awareness
based education. He has provided training for the US Naval Special Operations
Command, FBI National Citizens Academy Alumni Association, National Association
of School Resource Officers, and 'others'.

[Left Of Bang][amazonlob] is a manual for the US Marine Corps Combat Hunter
Program. This book is a detailed guide to learning to understand and trust your
gut when it comes to threats and how to respond to them.

[Patrick Van Horne and Jason A. Riley][authorslob] are the authors of Left Of
Bang. They provide training to the US Marine Corp, Law Enforcement, and
civilians on correct threat response.

## Conclusion

Violence is inevitable. There are an increasing number of people calling for
violence with an expectation that conflict resolution cannot be found within
nonviolent means with regards to current and future political as well as
religious differences. Targets are being chosen and planning is being conducted
right now by both groups as well as individuals from all spectrum of the
community. You cannot allow yourself to become complacent or to be caught
unaware. 

## Final Recommendations

1. Prepare yourself mentally for violence and how you will respond.

2. Actively decide to defend yourself and your family.

3. Purchase a tourniquet and other first aid supplies. Learn to use it.

4. Understand the phrase 'The New Normal'.

5. Seek out additional instruction.

[isisthreat]: /../assets/images/inserts/03232019-church/isis_temple_attack.png "Isis Temple Threat"
[isisweapon]: /../assets/images/inserts/03232019-church/isis_weapon.jpg "Isis Weapon Threat"
[tarrant]: /../assets/images/inserts/03232019-church/tarrant_spray.jpg "Tarrant Wall Art"
[tarrantweap]: /../assets/images/inserts/03232019-church/rifle_tarrant.jpg "Tarrant Weapon Art"
[tarrantmags]: /../assets/images/inserts/03232019-church/magazines_tarrant.jpg "Tarrant Magazine Art"
[authortarani]: https://stevetarani.com/ 'The Author Steve Tarani'
[amazontarani]: https://www.amazon.com/Your-most-Powerful-Weapon-Using/dp/B078YQ2D2F 'Amazon Link' 
[amazonlob]: https://www.amazon.com/Left-Bang-Marine-Combat-Program/dp/1936891301 'Amazon Link'
[authorslob]: http://cp-journal.com/ 'The left of bang journal'
