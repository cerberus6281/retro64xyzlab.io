---                                                                             
layout: post
title: "Book Study - Rodrigo Duterte: 16th Philippines President"
date: 2018-11-25T15:52:43-07:00
author: RetroMe
summary: >
  Due to the number of controversies that surround the current Philippine
  President and with urging by family members I began researching Rodrigo
  Duterte. One of the books I began reading promises not to be a politically
  motivated writing but an explanation of some of the history surrounding the
  election of Mr. Duterte during his presidential run.
categories: books 
thumbnail: fa-book
tags:
 - book review
 - politics
 - global
 - philippines
---

### A foreword

This book was given to me as a gift and due to the compact nature of the book I
was able to read it within an hour or two. This book promised to be a
non-political explanation of some of the events surrounding the election of
Rodrigo Duterte to the Presidency of the Philippines.

## Rodrigo Duterte: 16th Philippines President

**The most hardworking yet, controversial President**

This book was written by Frealyn M. Navarro and Sofriano Reign III. It opens
with a summary that posits questions that the author wished to answer in her
research of the political figure Rodrigo Roa Duterte. Some of these questions
include his education, his policy, his public persona, and his effect on the
war on drugs within the Philippines.

[Rodrigo Roa Duterte][wiki] is the 16th president of the Philippines. He comes
from Mindanao which is the southernmost major island in the country. President
Duterte is a graduate of Lyceum of the Philippines University and received a
law degree from San Beda College of Law. He held the position of Mayor in Davao
City on Mindanao island for nearly 22 years.

## Election

President Duterte ran on several policies. His main policies included Crime and
Corruption, Inclusive Economic Changes, and Equal Protections under the law. He
promised to end crime in the Philippines through the institution of the death
penalty for heinous crimes as well as by increasing the salaries of policemen
within the nation. He also pushed to increase the number of jobs available in
the countrysides of the nation, reduces taxes on the poor, and end the practice
of contractualization.

President Duterte received 16,601,997 votes. This was 6.6 million more votes
than his closest competitor. His trust rating from [Pulse Asia][pulse] was 91%
shortly after his election. As of July of 2018, his trust rating is 87% and his
approval rating is 88%.

### What is Contractualization?

In the Philippines it is common practice to hire employees on a temporary
basis. The period of employment generally lasts five months because the
Philippines has laws that state that any employee working for six months must
become a regularized employee. Therefore the business will hire, fire, and
rehire employees repeatedly in order to avoid paying for benefits and bonuses
such as sick leave, vacation, meals, educational assistance, or health
insurance.

## Criticism

President Duterte was rebuked by the media for his harsh stance on drug crime,
Catholic Church crimes, and his anti President Obama rhetoric. He has been
quoted calling Pope Francis and Former President Barack Obama a 'son of a
whore'. He has made claims that he has personally executed criminals. He has
claimed to have several mistresses. He has also been quoted as urging Filipino
citizens to kill drug addicts while also urging Communist Rebels like the New
Peoples Army to 'use your kangaroo courts to kill them to speed up the solution
to our problem'.

## Popularity

President Duterte enjoys immense popularity among the common peoples of the
Philippines. He is praised by internal media for his tough stance of crime and
his unwillingness to allow his nation to become a narco state. He regularly
advocates for the poor. 

## Conclusion

President Duterte is an extremely popular figure with many individuals I have
spoken to. He is known for holding a hard line against drugs and violence and
his inflammatory rhetoric is respected by many peoples within the Philippines.
It would appear that he has lived a very interesting life and worked
diligently in an attempt to improve his country if the books written about him
are to be believed. Regardless of the opinion of the west, I think he holds a
lot of support from his constituents and his style of leadership is becoming
increasingly popular amongst the populace.

You can purchase the book on [Amazon][amazon].

[amazon]: https://www.amazon.com/RODRIGO-DUTERTE-Philippines-hardworking-controversial-ebook/dp/B073D1JS8F/ref=sr_1_1?ie=UTF8&qid=1543156528&sr=8-1&keywords=rodrigo+duterte%3A+16th+philippines 'Amazon Books' 
[wiki]: https://en.wikipedia.org/wiki/Rodrigo_Duterte 'Wiki Duterte'
[pulse]: http://www.pulseasia.ph/ 'Opinion Polls Asia'
