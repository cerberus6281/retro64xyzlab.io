---
layout: post
title: "Using GNU IceCat as an alternative Tor browser."
date: 2018-10-01T04:52:43-07:00
author: RetroMe
summary: >
  GNU IceCat is a GNU version of Firefox. You may know that Firefox is the base
  on which the Tor browser is built but can the ethical advantages of IceCat
  provide enhanced security over the traditional Tor browser? I think so.
categories: software
thumbnail: fa-microchip
tags:
 - software
 - icecat
 - tor
 - tor-browser
 - firefox
 - how to
---

# What is GNU IceCat?

GNU IceCat is the GNU version of the Firefox browser. The main advantage of
IceCat is an ethical one because it is free software. Firefox by Mozilla
distributes non-free software as plug-ins and addons. The Tor browser, built on
Firefox, is potentially harmful to your freedom.

## Why IceCat?

The Tor browser bundle is [extremely vulnerable][fhtakedown]. We have seen time
and time again that users are identified. There are numerous examples of users
being identified through JavaScript powered malware. IceCat is a good
alternative for the Tor browser in my opinion.

## Installing

```
$ yaourt -S icecat
```

Arch and Manjaro package manager `yaourt` greatly simplifies the installation
of IceCat. Some distributions will require that you build IceCat from source.
This is beyond the scope of this blog post. You can also pull a copy from the
[gnu ftp][gnuftp]. Remember to get the latest version.

## Setup

Setup is a breeze.

SSH into your Tor box. You aren't running Tor locally are you?
[Watch my video on Tor][torvid]. Done? Good. Now go build a VPS and install Tor
on that. Once you have Tor installed we can connect.

```
$ ssh -L 9050:127.0.0.1:9050 user@1.1.1.1
```

We want to expose port 9050 to ourselves on localhost from our VPS.

Now open IceCat and choose the following privacy settings on launch.

1. [Disable JavaScript][jtrap]
2. Do not load custom fonts
3. Tracking protection
4. Isolate requests to First Party domains
5. Spoof Referers (sic)
6. Block third-party requests

Make sure that Enable Geolocation and Enable WebGL are disabled.

It will look like this -

![IceCat privacy settings][privacyimage]

Now open Preferences->General->Network Proxy

Let's setup a manual proxy configuration by choosing that option. Then fill out
the SOCKS Host box with 127.0.0.1 and port 9050. Check mark the Proxy DNS when
using SOCKS v5.

It will look like this -

![Proxy Settings For IceCat][proxyimage]

## Test It All!

1. Go [here][tortest] in your new secure Tor Browser. It should report a
   Congratulations message.

2. Try the following command in a terminal.

```
$ curl --socks5 localhost:9050 --socks5-hostname localhost:9050 -s https://check.torproject.org/ | cat | grep -m 1 Congratulations | xargs
```

You should see text that states 'Congratulations. This browser is configured to
use Tor.' You can now be confident that data sent over port 9050 when connected
to your VPS will go through the Tor network.

## Am I an elite invisible hacker who can never be caught now?

No.

## Threats?

Absolutely. Even [css][cssthreat] can be a potential vulnerability. Turning off
JavaScript will remove a wide array of threats but html and css can still
potentially be a method by which a browser attack can be executed. Don't forget
to implement `firejail` and `apparmor` in combination with other necessary
behavior modifications depending on your threat matrix.

[fhtakedown]: http://archive.is/t7RHy 'FBI Malware For Tor Browser'
[gnuftp]: https://ftp.gnu.org/gnu/gnuzilla/ 'Latest Builds Of IceCat'
[torvid]: https://retro64xyz.gitlab.io/presentations/2018/04/19/introduction-to-tor/ 'Tor Video'
[proxyimage]: /../assets/images/inserts/10012018-icecat/icecat-proxy.png
[privacyimage]: /../assets/images/inserts/10012018-icecat/icecat-priv.png
[tortest]: https://check.torproject.org/ 'Test Your Browser'
[cssthreat]: https://bugzilla.mozilla.org/show_bug.cgi?id=574059 'CSS vulnerability in Firefox'
[jtrap]: http://archive.is/26tUf 'The JavaScript Trap'
