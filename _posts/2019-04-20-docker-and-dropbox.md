---
layout: post
title: "How do I use Docker and Dropbox to run multiple instances?"
date: 2019-04-19T15:52:43-07:00
author: RetroMe
summary: >
  You may be curious how to run Docker and Dropbox to run multiple Dropbox
  instances on the same computer. Docker makes it very easy to interact with
  mutiple Dropbox accounts.
categories: how-to
thumbnail: fa-microchip
tags:
 - how-to
 - docker
 - dropbox
 - linux
---

## Settin Up Docker And Dropbox

[Shivam Kapoor][shiva] created an excellent blog post on how to setup Dropbox
using Docker to allow multiple instances to function on a system. I made some
minor changes and used this to setup a laptop so I can work with multiple
copies of Dropbox without issue.

## How To

1. Install Docker.
	```
	$ sudo pacman -S docker
	```
	Use your package manager to install [Docker](https://www.docker.com/).
2. Setup your user.
	```
	$ sudo usermod -aG docker $USER
	```
	Setup your user by adding them to the `docker` group. This is so you don't
	have to use `sudo` to work with Docker.
3. Get the Docker Image
	```
	$ docker pull janeczku/dropbox
	```
	User janeczku created a docker image that runs dropbox. Review the 
	[code yourself](https://hub.docker.com/r/janeczku/dropbox/).
4. Create the Docker Machine and the volume
	```
	$ docker run -d --restart=always --name=RENAMETHIS -v /home/aaronj/Dropbox:/dbox/Dropbox -v /home/aaronj/.dropbox:/dbox/.dropbox -e DBOX_UID=1000 -e DBOX_GID=1000 janeczku/dropbox
	```
	You need to edit the name of the machine, the locations you want to use for
	the volumes, and the UID/GID so you don't run into permissions issues. If
	your user is `aaronj` you would do `cat /etc/passwd | grep "aaronj"` and
	use the UID/GID displaye so you don't run into permissions issues. If your
	user is `aaronj` you would do `cat /etc/passwd | grep "aaronj"` and use the
	UID/GID displayed.
5. Register the Docker Account
	```
	$ docker logs RENAMETHIS
	```
6. Look for a line that says something like -
	```
	https://www.dropbox.com/cli_link_nonce?nonce=612531228134bf411986991sdd4d69438
	```
Click it and login to register your Dropbox account with that container.
7. Restart the docker machine
	```
	$ docker restart RENAMETHIS
	```
8. Wait for the box to sync.
9. Fix the file permissions.
	```
	$ sudo chmod 755 /home/aaronj/Dropbox -R
	$ sudo chmod g+s /home/aaronj/Dropbox -R
	$ sudo chown aaronj:aaronj /home/aaronj/Dropbox -R
	```

This resolved my issue and with the laptop being a single user system, I am
unconcerned about the permissiveness of the settings because my system is a
single user box with disk encryption. You may want to write a cron to restart
the box every 10-15 minutes as well. The Dropbox daemon seems to have some
issues within the container and it helps to just restart the container.

### Troubleshooting

1. How do I restart my dropbox machine?
```
$ docker container ls
```
You will want to find the name of the container you need.
2. Then restart the machine.
```
$ docker restart NAMEOFTHEMACHINE
```

This usually solves the issue. You can also create a cronjob that restarts
regularly to solve any issues off the bat.

```
$ sudo crontab -s
# Edit the crontab to look like this -

# This restarts the container every 10 minutes. Edit this as you see fit.
*/10 * * * * /usr/bin/docker restart RENAMETHIS
```

[shiva]: 'http://www.shivamkapoor.com/blogs/technology/2018/08/16/configure-unlimited-dropbox-accounts-on-single-pc-seamlessly-using-docker/' 'Setting Up Docker Dropbox'
